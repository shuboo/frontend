export default {
  SET_PRODUCTS(state, data) {
    state.products = data;
  },
  SET_DELETE_PRODUCTS(state, dProducts) {
    state.dProducts = dProducts;
  },
  SET_ERRORS(state, errors) {
    state.errors = errors;
  },
  SET_NULL(state) {
    const addProduct = {
      sku: null,
      name: null,
      price: null,
      weight: null,
      height: null,
      length: null,
      size: null,
      width: null,
      type: "book",
    };
    state.addProduct = addProduct;
  },
};
